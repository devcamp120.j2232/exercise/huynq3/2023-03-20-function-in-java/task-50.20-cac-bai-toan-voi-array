import java.util.Arrays;
import java.util.List;
import java.util.ArrayList;
import java.util.Random;

public class Task5020 {
    public static void main(String[] args) throws Exception {
        System.out.println("Hello, World!");

        String arr1 = "Devcamp";
        System.out.println(subTask1(arr1) + "=> Array");

        int[] arr2 = { 1, 2, 3 };
        System.out.println(subTask1(arr2) + "=> Array");

        String[] arr3 = "Devcamp User".split(" ");
        System.out.println(subTask1(arr3) + "=> Array"); // true

        // trả về phân tử index n trong mảng
        int[] arr4 = { 1, 2, 3, 4, 5, 6 };
        subTask2(arr4, 3);
        subTask2(arr4, 6);

        // sắp mảng tăng dần
        int[] arr5 = { 3, 8, 7, 6, 5, -4, -3, 2, 1 };

        subTask3(arr5); // sắp xếp mảng theo thứ tự tăng dần

        // tìm vị trí index của phần tử n trong mảng
        int[] arr6 = { 1, 2, 3, 4, 5, 6 };
        subTask4(arr6, 3);
        subTask4(arr6, 7);

        // nối 2 mảng
        int[] arr7 = { 1, 2, 3 };
        int[] arr8 = { 4, 5, 6 };
        int[] newArr = subTask5(arr7, arr8);
        System.out.println("Mảng ghép được là: " + Arrays.toString(newArr)); // nhớ cái hàm này quan trọng, import thư
                                                                             // viện nữa
        //subtask 6 in ra mảng mới String or Number
        Object[] arr9={ 0, 15, false, -22, "html", true, "develop", 47, null };
        Object[] newArrNumString= subTask6(arr9);            
        System.out.println(Arrays.toString(newArrNumString));
        //sub7
        int[] arr10={2, 5, 9, 6};
        int[] arr11={2, 9, 6};
        int[] newArr10Without5= subTask7(arr10, 5);
        int[] newArr11Without5= subTask7(arr11, 5);
        int[] newArr10Without6= subTask7(arr10, 6);

        System.out.println(Arrays.toString(newArr10Without5));
        System.out.println(Arrays.toString(newArr11Without5));
        System.out.println(Arrays.toString(newArr10Without6));

        //sub 8 lấy random 1 phần tử
        int[] arr12={1,2,3,4,5,6,7,8,9};
        int newElement1=subTask8(arr12);
        int newElement2=subTask8(arr12);
        int newElement3=subTask8(arr12);
        System.out.println("Random lần 1: "+newElement1);
        System.out.println("Random lần 2: "+newElement2);
        System.out.println("Random lần 3: "+newElement3);

        //sub 9: tạo 1 mảng gồm x phần tử có giá trị y
        int[] arrX_Y = subTask9(6, 0);
        System.out.println(Arrays.toString(arrX_Y));
        System.out.println(Arrays.toString(subTask9(4, 11)));

        //sub 10: tao mảng có y số liên tiếp bắt đàu là x
        System.out.println(Arrays.toString(subTask10(1,4)));
        System.out.println(Arrays.toString(subTask10(-6, 4)));
    }


    

    public static void subTask4(int[] arr, int n) {
        int index = Arrays.binarySearch(arr, n);
        if (index < 0) {
            System.out.println("Subtask 4: Phần tử có giá trị " + n + " không tìm được vị trí trong mảng");
        } else {
            System.out.println("Subtask 4: Vị trí của phần tử " + n + " trong mảng là " + index);
        }
    }

    public static boolean subTask1(Object obj) {
        return obj.getClass().isArray();

    }

    // Subtask 2 hàm trả về phần tử thứ n của mảng
    public static void subTask2(int[] arr, int n) {

        if (n >= 0 && n < arr.length) {

            System.out.println("Phần tử thứ " + n + " trong mảng là: " + arr[n]);
        } else {

            System.out.println("Phần tử thứ " + n + " kiếm hổng ra ");
        }
    }
    
    public static void subTask3(int[] arr) {
        Arrays.sort(arr); // Sort mảng
        System.out.print("Mảng đã sắp xếp theo thứ tự tăng dần: [");
        for (int i = 0; i < arr.length; i++) {
            System.out.print(arr[i] + ", "); // in ra các phần tử trong mảng đã sắp xếp

        }
        System.out.println("]");
    }

    public static int[] subTask5(int[] arr1, int[] arr2) {
        int[] newArr = new int[arr1.length + arr2.length]; // khai báo độ dài mảng mới
        int index = 0;
        for (int i = 0; i < arr1.length; i++) {
            newArr[index++] = arr1[i]; // đưa mảng 1 vào
        }
        for (int i = 0; i < arr2.length; i++) {
            newArr[index++] = arr2[i]; // đưa mảng 2 vào
        }
        return newArr;

    }

    public static Object[] subTask6(Object[] arr) {
        List<Object> list = new ArrayList<>();
        for (Object obj : arr) {
            if (obj instanceof String || obj instanceof Number) {
                list.add(obj);
            }
        }
        Object[] newArrNumString = new Object[list.size()];
        newArrNumString = list.toArray(newArrNumString);
        return newArrNumString;
    }

    public static int[] subTask7(int[] arr, int n) {
        int count = 0;
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] == n) {
                count++;
            }
        }
        int[] newArr = new int[arr.length - count];
        int j = 0;
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] != n) {
                newArr[j] = arr[i];
                j++;
            }
        }
        return newArr;
    }
    
    public static int subTask8(int[] arr) {
        Random rand = new Random();
        int index = rand.nextInt(arr.length);
        return arr[index];
    }
    public static int[] subTask9(int x, int y) {
        int[] arr = new int[x];
        for (int i = 0; i < x; i++) {
            arr[i] = y;
        }
        return arr;
    }

    public static int[] subTask10(int x, int y) {
        int[] arr = new int[y];
        for (int i = 0; i < y; i++) {
            arr[i] = x + i;
        }
        return arr;
    }

}
